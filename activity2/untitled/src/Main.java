import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        int[] intArray = {2, 3, 5, 7, 11};
        System.out.println("The first prime number is " + intArray[0]);
        System.out.println("The second prime number is " + intArray[1]);
        System.out.println("The third prime number is " + intArray[2]);
        System.out.println("The fourth prime number is " + intArray[3]);
        System.out.println("The fifth prime number is " + intArray[4]);

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory= new HashMap<String, Integer>()
        {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("Our current inventory consists of: " + inventory);
    }
}